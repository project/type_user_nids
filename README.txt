
Type User Nids Module
---------------------
Author: Ken Dawber
  (Drupal.org user Ausvalue)
   www.ausvalue.com


The Type user nids module creates a unid token -  The unid provides each node a unique number that's consecutive for the nodes created by this author of this type.

Similar to Type local nids module except the unid is numbered for each user.  Generally used with the Token module (required) and the Pathauto module.  This module allows you to consecutively number nodes of a particular type authored by a single user.

For example:
Assume you've created a form for users to fill out for each of their support requests.  This 'request' form is a new Drupal node type.  If a user named userfoo has 2 requests then the URLs for these could be consecutively numbered similar to the following:

www.example.com/request/userfoo/1
www.example.com/request/userfoo/2

and if userbar has 9 requests then the URLs for these could be consecutively numbered similar to the following:

www.example.com/request/userbar/1
www.example.com/request/userbar/2
... to
www.example.com/request/userbar/9

The above assumes that Pathauto is used to create an alias for all request node types similar to the following:

 request/[author-name]/[unid]

As used in the previous example, this module just helps give Drupal installations a more professional look and feel.  When used within the Pathauto module the unid token is called [unid].

The module includes views2 integration.  As well as allowing node based listings to show the nodes unid this also allows creating reports on number of nodes authored of each type by each author.

The unid may also be useful in other modules such as computed_field, uploadpath, filefield, contemplate, tokenize, custom_breadcrumbs etc.  It is expected to be useful for other applications such as generating numbers for invoices etc

FAQ.
---

Q. Where's the administration panel for this module?
A. There isn't one.  Once the module's enabled it immediately starts working.

Q. What happens with all the nodes that are already part of the database before this Type user nids module is installed?
A. Whenever one of these nodes is accessed, (viewed or updated), the Type user nid module notes that the node doesn't have a unid and assigns a unid to it.

Q. Does this mean that for nodes created previous to Type user nids being installed, the numbering of the unids will not be in order of when the node is created.
A. Yes.  The order will simply be the order that they're next accessed.

Q.  Is there any easy way to get the unids numbered in the order the nodes were created.
A.  Yes.  Directly after installing Type User nids, go to the administration page for Pathauto.  Fill in node path settings for each node type using an appropriate pattern that includes the [unid].  Select 'Bulk generate aliases for nodes that are not aliased' and click the 'Save configuration' button.  As this generates aliases, it will create unids for all nodes without a unid.  The current version of Pathauto performs this in the numeric order of the nids.  Note: It is best to do this when the cron job is not expected to run.  Cron may access various nodes.

Q.  If I delete all aliases, will this delete the generated unids.
A.  No.  The unids generated will remain in the database regardless of deleting aliases.

Q.  How do I delete all the generated unids.
A.  Note:  Before deleting the unids you should delete all objects such as aliases that use them.  To delete all unids, disable the Type user nids module (admin/build/modules) and then uninstall (admin/build/modules/uninstall) the Type user nids module.

Q.  Is the format of the unid token always [unid]?
A.  No.  The format (beginning and ending characters) is up to the module that you're using the token within.  For example it could be %unid or %unid% or <unid>.



Views 2 Integration Usage
-----------------------

A) Usage for User nids

For normal usage within Views 2 you would start by clicking on the 'Add' tab.  You have a number of Type options here.  Normally you would select 'Node'.

When you initially go into the edit window of Views and add fields you will have extra fields available under the group 'Type user nids'.  You would normally just use the unid field.

B) Usage for reporting number of nodes of each type created by each author

Since the Type user nids module contains a database table that effectively gives the total number of nodes that each author has created of each node type, it is  expected that this could be useful information for reports etc.  Adding this into the views integration was difficult.  The table relates to Drupal's node_type table but the node_type table is currently not integrated in Views implementations.  Consequently, this integration required integrating most of Drupal's node_type table as well.

Usage is different to that for User nids.

When you click on the Add tab, included at or near the bottom of the Type options is a new type called 'Number unids allocated'.   Select this.   When you go into the edit window of Views, to get fields from the users table you need to add a relationship to the 'User'.  Relationships is at the top in the same column as the fields.  After you have added the relationship to the 'User' you will have access to the num_unids field along with fields from both the node_type table and the users table.  The num_unids is the number of unids that have been assigned for this author of this type.

An alternative way to produce information on number of unids per author is to select User as the type from the Add tab.  This will allow you to produce a similar listing of nodes created per author per type but doesn't have access to further information from the node_type table.  Use the 'Type user nids: Type' for the Type field.  The listing is slightly different to that produced with the 'Number unids allocated' type in that it includes users that have never authored a node.


Possible Future Additions/Changes
---------------------------------

Addition of a Next unid [next_unid] variant for use with the auto-nodetitle module.  At the time the auto-nodetitle module is run, the nid is not yet known so normal calculation of a unid isn't possible.  The supply of the next number that will be assigned for the current author and type may be used instead.

As opposed to the current Type Local Nids module I'm anticipating including an admin page.

One aspect that may be configured is which node types this is to be enabled for.  For normal page views, this isn't required as only a single indexed database access is required and even this may disappear in the future. (see below).  It would only be useful for sites that had high numbers of nodes being created such as an invoicing/accounting application.

The administration panel may also be useful for other aspects that can be configured such as the initial number (or offset) to start numbering from.

Changes to the Database could occur where the node_unid table is incorporated into Drupal's node table.  If a unid field was added to the node table, then use of the unid token would require no additional accesses to the database for normal page views.

Local nids could also be incorporated so that generating both the local nid and the user nid tokens was performed for the same database access.

Create a generate all unids task that runs when the module is first installed.  This would ensure that the unids created were all consecutive.

