<?php

/**
 * @file
 *  This file is used to tell the views module about the new number_unids
 *         and node_unid tables.
 *    We also use it to tell the views module about the node_type table.
 *
 * Database definition:
 * @code
 *   CREATE TABLE number_unids (
 *     uid int(10) unsigned NOT NULL,
 *     type int(10) unsigned NOT NULL,
 *     num_unids int(10) unsigned NOT NULL,
 *     PRIMARY KEY (uid, type)
 *   )
 *   CREATE TABLE node_unid (
 *     nid int(10) unsigned NOT NULL,
 *     unid int(10) unsigned NOT NULL,
 *     PRIMARY KEY (nid)
 *   )
 *   CREATE TABLE node_type (
 *     type varchar(32) NOT NULL default '0',
 *     module varchar(32) NOT NULL default '0',
 *     description varchar(32) NOT NULL default '0',
 *     help varchar(32) NOT NULL default '0',
 *     has_title tinyint(3) unsigned NOT NULL default '0',
 *     title_label varchar(32) NOT NULL default '0',
 *     has_body tinyint(3) unsigned NOT NULL default '0',
 *     body_label varchar(32) NOT NULL default '0',
 *     min_word_count tinyint(35) unsigned NOT NULL default '0',
 *     (plus other fields we aren't bothering with),
 *     PRIMARY KEY (type)
 *   )
 * @endcode
 */

function type_user_nids_views_data()  {
  // Basic table information.
  $data = array();

  // ----------------------------------------------------------------
  //  New groups within Views called 'Type user nids' and 'Node type'
  $data['number_unids']['table']['group']  = t('Type user nids');
  $data['node_unid']['table']['group']  = t('Type user nids');
  $data['node_type']['table']['group']  = t('Node type');

  // tables + fields that can be used for SQL Joins to the number_unids table.
  //   Joins described for joins to the node_type and the node tables.
  $data['number_unids']['table']['join'] = array(
    'node_type' => array(
      'left_field' => 'type',
      'field' => 'type',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
    'node' => array(
      'left_field' => 'type',
      'field' => 'type',
     ),
  );

  // tables + fields that can be used for SQL Joins to the node_unid table
  //   Joins described for a join to  the node table.
   $data['node_unid']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  
  // tables + fields that can be used for SQL Joins to the node_type table
  //   Joins described for a join to  the node table.
   $data['node_type']['table']['join'] = array(
    'node' => array(
      'left_field' => 'type',
      'field' => 'type',
    ),
    'number_unids' => array(
      'left_field' => 'type',
      'field' => 'type',
    ),
    'users' => array(
      'left_table' => 'number_unids',
      'left_field' => 'uid',
      'field' => 'type',
    ),
  );

  // num_unids field from the number_unids table
  $data['number_unids']['num_unids'] = array(
    'title' => t('Number of unids'),
    'help' => t('Number of unids assigned for this type for this author'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Type field from the number_unids table		
  $data['number_unids']['type'] = array(
    'title' => t('Type'),
    'help' => t('Node type.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'argument' => array(
       'handler' => 'views_handler_argument_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
  );
  
  // unid field from the node_unid table		
  $data['node_unid']['unid'] = array(
    'title' => t('unid'),
    'help' => t('unid for this node.'),

    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'argument' => array(
       'handler' => 'views_handler_argument_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
  );

  // Type field from the node_type table		
  $data['node_type']['type'] = array(
    'title' => t('Type'),
    'help' => t('Node type.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'argument' => array(
       'handler' => 'views_handler_argument_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
  );

  // Name field from the node_type table		
  $data['node_type']['name'] = array(
    'title' => t('Name'),
    'help' => t('Name of Node Type.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'argument' => array(
       'handler' => 'views_handler_argument_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
  );

  // Module field from the node_type table		
  $data['node_type']['module'] = array(
    'title' => t('Module'),
    'help' => t('Node Types Module.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'argument' => array(
       'handler' => 'views_handler_argument_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
  );

  // Description field from the node_type table		
  $data['node_type']['description'] = array(
    'title' => t('Description'),
    'help' => t('Description of Node Type.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'argument' => array(
       'handler' => 'views_handler_argument_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
  );

  // Help field from the node_type table		
  $data['node_type']['help'] = array(
    'title' => t('Help'),
    'help' => t('Help for Node Type.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'argument' => array(
       'handler' => 'views_handler_argument_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
  );

  // has_title field from the node_type table		
  $data['node_type']['has_title'] = array(
    'title' => t('Has title'),
    'help' => t('Indicator of whether this node type has a title.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


  // Title_label field from the node_type table		
  $data['node_type']['title_label'] = array(
    'title' => t('Title label'),
    'help' => t('Title label for Node Type.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'argument' => array(
       'handler' => 'views_handler_argument_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
  );

  // has_body field from the node_type table		
  $data['node_type']['has_body'] = array(
    'title' => t('Has body'),
    'help' => t('Indicator of whether this node type has a body.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Body_label field from the node_type table		
  $data['node_type']['body_label'] = array(
    'title' => t('Body label'),
    'help' => t('Body label for Node Type.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'argument' => array(
       'handler' => 'views_handler_argument_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
  );

  // min_word_count field from the node_type table		
  $data['node_type']['min_word_count'] = array(
    'title' => t('Min word count'),
    'help' => t('Minimum word count for the body of this node type.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  //  Use number_unids as a new base table
  //     by creating a new views type called 'Number unids allocated'
  //  This allows it to be selected as the 'view type'
  //          when you initially add a new view. 
  $data['number_unids']['table']['base'] = array(
    'field' => 'type',
    'title' => t('Number unids allocated'),
    'help' => t("Number of unids allocated per author per type."),
    'weight' => 9,
  );

  // When using the new 'Number unids allocated' type you need to use relationships
  //   to access fields in other tables.

  // Relationship to the 'users' table
  $data['number_unids']['uid'] = array(
    'title' => t('User'),
    'help' => t('The particular author this number of unids relates to'),
    'relationship' => array(
      'label' => t('Author'),
      'base' => 'users',
      'base field' => 'uid',
      // This allows us to not show this relationship if the base is already
      // users so users won't create circular relationships.
      'skip base' => array('users'),
    ),
  );

  return $data;
}
